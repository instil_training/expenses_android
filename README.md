## Expenses for Android

This is the Expenses app used in the Instil Android workshop. It is intended to cover enough of the basic functionality of an Android app to give the user a solid introduction to app development on the Android platform.
 
Expenses is a simple app that manages expense reports. You can add, edit and remove expense reports from the initial list view. Receipts are added to expense reports with the ability to scan a receipt and send it to a backend service to be read and processed.

The opportunity was also taken to demonstrate the use of several third-party libraries.

### Overview

The app stores all data locally on the device, unlike a production app that would simply cache remote data on the device. [Realm](https://realm.io/products/realm-mobile-database/) is used for data storage. Check our the `data` and `model` packages to see the data support. Note, the `data` package contains two implementations of the `ExpenseDataStore` interface, one backed by Realm, the other backed by a simple `List<Expense>`, just to show the utility of separating your data layer, even in a mobile device.

Communication with the backend service is managed by [Retrofit](https://square.github.io/retrofit/). Check out the `network` package to see how that is coded. 

As an example of how to manage dependencies in your apps, the `ReceiptReadService` instance is injected into the app using [Dagger](https://google.github.io/dagger/), a dependency injection tool developed by Google. If you check out the `ExpensesApplication` class, you'll see another example of how to make your service classes available through accessors on your app's application instance.

The [ButterKnife](https://jakewharton.github.io/butterknife/) library is used throughout the app to easily bind views to their ids in the layou files. Look for the `@BindView` annotation to see how this works.

Checkout the `settings` package to see an example of how to configure and manage application settings.

Finally, to capture the images of receipts in order to send them to the backend, we simply use the system camera application rather than custom camera functionality. This is done in the `ReceiptDetailActivity` class.