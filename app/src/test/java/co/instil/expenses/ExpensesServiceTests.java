package co.instil.expenses;

import co.instil.expenses.data.ExpenseDataStore;
import co.instil.expenses.data.ExpensesService;
import co.instil.expenses.model.Expense;
import com.google.common.collect.Lists;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ExpensesServiceTests {

    private ExpenseDataStore dataStore = mock(ExpenseDataStore.class);
    private ExpensesService target = new ExpensesService(dataStore);

    @Test
    public void shouldReturnCountOfExpensesFromDataStore() {
        when(dataStore.count()).thenReturn(5);
        assertThat(target.numberOfExpenses()).isEqualTo(5);
    }

    @Test
    public void shouldFetchExpensesFromDataStore() {
        setupDataStoreToReturnEmptyList();
        callGetExpensesFromService();
        verify(dataStore).all();
    }

    @Test
    public void shouldCacheExpenses() {
        setupDataStoreToReturnEmptyList();
        callGetExpensesFromService(2);
        verify(dataStore, times(1)).all();
    }

    @Test
    public void shouldInvalidateCacheWhenAddingNewExpense() {
        setupDataStoreToReturnEmptyList();
        callGetExpensesFromService(2);
        target.saveExpense(new Expense());
        callGetExpensesFromService();
        verify(dataStore, times(2)).all();
    }

    @Test
    public void shouldInvalidateCacheWhenDeletingAnExpense() {
        setupDataStoreToReturnEmptyList();
        callGetExpensesFromService(2);
        target.remove(new Expense());
        callGetExpensesFromService();
        verify(dataStore, times(2)).all();
    }

    private void callGetExpensesFromService() {
        callGetExpensesFromService(1);
    }

    private void callGetExpensesFromService(int times) {
        for (int count = times; count >= 0; count--) {
            target.getAllExpenses();
        }
    }

    private void setupDataStoreToReturnEmptyList() {
        when(dataStore.all()).thenReturn(Lists.<Expense>newArrayList());
    }

}
