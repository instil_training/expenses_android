package co.instil.expenses.dependency;

import co.instil.expenses.ReceiptDetailActivity;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface NetworkComponent {

    void inject(ReceiptDetailActivity activity);

}
