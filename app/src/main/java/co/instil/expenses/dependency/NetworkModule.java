package co.instil.expenses.dependency;

import android.content.SharedPreferences;
import co.instil.expenses.network.ReceiptReadService;
import co.instil.expenses.settings.SettingsActivity;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import javax.inject.Singleton;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    public static ReceiptReadService provideReceiptReadService(SharedPreferences sharedPreferences) {
        String baseUrl = sharedPreferences.getString(SettingsActivity.SERVICE_URL_KEY, "https://receipts.eu.ngrok.io");
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .build();
        return retrofit.create(ReceiptReadService.class);
    }

}
