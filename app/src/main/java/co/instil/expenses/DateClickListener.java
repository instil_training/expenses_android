package co.instil.expenses;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;

import java.util.Calendar;
import java.util.Date;

class DateClickListener implements View.OnClickListener {

    private final Context context;
    private final DatePickerDialog.OnDateSetListener dateSetListener;
    private final Date date;

    DateClickListener(Context context, DatePickerDialog.OnDateSetListener dateSetListener, Date date) {
        this.context = context;
        this.dateSetListener = dateSetListener;
        this.date = date;
    }

    @Override
    public void onClick(View v) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        new DatePickerDialog(
                context,
                dateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }
}
