package co.instil.expenses;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import co.instil.expenses.model.Expense;
import co.instil.expenses.model.Receipt;

class ReceiptsAdapter extends RecyclerView.Adapter {

    private final ReceiptAdapterDelegate delegate;
    private final Expense expense;

    public ReceiptsAdapter(ReceiptAdapterDelegate delegate, Expense expense) {
        this.delegate = delegate;
        this.expense = expense;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_receipt_item, parent, false);
        return new ReceiptViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Receipt receipt = expense.receiptForIndex(position);
        ((ReceiptViewHolder) holder).prepareForDisplay(receipt);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delegate.receiptWasSelected(receipt);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (expense != null) {
            return expense.numberOfReceipts();
        }
        return 0;
    }
}
