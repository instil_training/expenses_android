package co.instil.expenses;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.instil.expenses.model.Expense;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class ExpensesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.title_text_field) TextView titleView;
    @BindView(R.id.date_text_field) TextView dateView;
    @BindView(R.id.total_text_field) TextView totalView;

    private Expense expense;

    ExpensesViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    void prepareForDisplay(Expense expense) {
        this.expense = expense;
        titleView.setText(expense.getTitle());
        dateView.setText(formatDate(expense.getDate()));
        totalView.setText(formatAmount(expense.getTotal()));
    }

    private String formatDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM d, yyyy", Locale.getDefault());
        return simpleDateFormat.format(date);
    }

    private String formatAmount(Float amount) {
        return String.format(Locale.getDefault(), "£%.2f", amount);
    }

    public Expense getExpense() {
        return expense;
    }
}
