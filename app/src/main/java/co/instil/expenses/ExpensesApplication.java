package co.instil.expenses;

import android.app.Application;
import co.instil.expenses.data.ExpensesService;
import co.instil.expenses.data.RealmExpenseDataStore;
import co.instil.expenses.dependency.AppModule;
import co.instil.expenses.dependency.DaggerNetworkComponent;
import co.instil.expenses.dependency.NetworkComponent;
import io.realm.Realm;

public class ExpensesApplication extends Application {

    private ExpensesService expensesService;
    private NetworkComponent networkComponent;

    public ExpensesService getExpensesService() {
        return expensesService;
    }

    public NetworkComponent getNetworkComponent() {
        return networkComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        Realm realm = Realm.getDefaultInstance();
        expensesService = new ExpensesService(new RealmExpenseDataStore(realm));
        networkComponent = DaggerNetworkComponent.builder().appModule(new AppModule(this)).build();
    }
}
