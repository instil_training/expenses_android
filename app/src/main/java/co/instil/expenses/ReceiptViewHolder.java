package co.instil.expenses;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.instil.expenses.model.Receipt;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class ReceiptViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.location_text_field) TextView locationView;
    @BindView(R.id.date_text_field) TextView dateView;
    @BindView(R.id.amount_text_field) TextView amountView;

    private Receipt receipt;

    ReceiptViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void prepareForDisplay(Receipt receipt) {
        this.receipt = receipt;
        locationView.setText(receipt.getLocation());
        dateView.setText(formatDate(receipt.getDate()));
        amountView.setText(formatAmount(receipt.getAmount()));
    }

    public Receipt getReceipt() {
        return receipt;
    }

    private String formatAmount(Float amount) {
        return String.format(Locale.getDefault(), "£%.2f", amount);
    }

    private String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM, dd yyyy", Locale.getDefault());
        return dateFormat.format(date);
    }

}
