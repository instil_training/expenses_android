package co.instil.expenses.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Receipt extends RealmObject implements Serializable {

    @PrimaryKey
    private String id = UUID.randomUUID().toString();
    private String location = "Unknown";
    private Date date = new Date();
    private Float amount = 0f;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Receipt)) {
            return false;
        }
        Receipt otherReceipt = (Receipt) other;
        return id.equals(otherReceipt.getId());
    }
}
