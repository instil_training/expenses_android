package co.instil.expenses.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Expense extends RealmObject implements Serializable {

    @PrimaryKey
    private String id = UUID.randomUUID().toString();
    private String title = "Untitled";
    private Date date = new Date();
    private RealmList<Receipt> receipts = new RealmList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int numberOfReceipts() {
        return receipts.size();
    }

    public float getTotal() {
        float total = 0f;
        for (Receipt receipt: receipts) {
            total += receipt.getAmount();
        }
        return total;
    }

    public void remove(Receipt receipt) {
        receipts.remove(receipt);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Expense)) {
            return false;
        }
        Expense otherExpense = (Expense) other;
        return id.equals(otherExpense.getId());
    }

    public Receipt receiptForIndex(int index) {
        return receipts.get(index);
    }

    public void addOrUpdate(Receipt receipt) {
        if (receipts.contains(receipt)) {
            int index = receipts.indexOf(receipt);
            receipts.set(index, receipt);
        } else {
            receipts.add(receipt);
        }
    }
}
