package co.instil.expenses;

import co.instil.expenses.model.Expense;

interface ExpensesAdapterDelegate {

    void expenseWasSelected(Expense expense);

}
