package co.instil.expenses.data;

import co.instil.expenses.model.Expense;

import java.util.List;

public interface ExpenseDataStore {

    int count();

    List<Expense> all();

    void save(Expense expense);

    void remove(Expense expense);

    Expense byId(String id);

}
