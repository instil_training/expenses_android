package co.instil.expenses.data;

import co.instil.expenses.model.Expense;

import java.util.ArrayList;
import java.util.List;

public class InMemoryExpenseDataStore implements ExpenseDataStore {

    private List<Expense> expenses = new ArrayList<>();

    @Override
    public int count() {
        return expenses.size();
    }

    @Override
    public List<Expense> all() {
        return expenses;
    }

    @Override
    public void save(Expense expense) {
        if (expenses.contains(expense)) {
            int index = expenses.indexOf(expense);
            expenses.set(index, expense);
        } else {
            expenses.add(expense);
        }
    }

    @Override
    public void remove(Expense expense) {
        expenses.remove(expense);
    }

    @Override
    public Expense byId(String id) {
        return null;
    }
}
