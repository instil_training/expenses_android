package co.instil.expenses.data;

import co.instil.expenses.model.Expense;
import io.realm.Realm;
import io.realm.RealmQuery;

import java.util.List;

public class RealmExpenseDataStore implements ExpenseDataStore {

    private Realm realm;

    public RealmExpenseDataStore(Realm realm) {
        this.realm = realm;
    }

    @Override
    public int count() {
        RealmQuery<Expense> query = realm.where(Expense.class);
        return (int) query.count();
    }

    @Override
    public List<Expense> all() {
        RealmQuery<Expense> query = realm.where(Expense.class);
        return realm.copyFromRealm(query.findAll());
    }

    @Override
    public void save(Expense expense) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(expense);
        realm.commitTransaction();
    }

    @Override
    public void remove(Expense expense) {
        realm.beginTransaction();
        queryForId(expense.getId()).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    @Override
    public Expense byId(String id) {
        Expense expense = queryForId(id).findFirst();
        return realm.copyFromRealm(expense);
    }

    private RealmQuery<Expense> queryForId(String id) {
        return realm.where(Expense.class).equalTo("id", id);
    }
}
