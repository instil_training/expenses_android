package co.instil.expenses.data;

import co.instil.expenses.model.Expense;

import java.util.List;

public class ExpensesService {

    private ExpenseDataStore dataStore;
    private List<Expense> expenses;

    public ExpensesService(ExpenseDataStore dataStore) {
        this.dataStore = dataStore;
    }

    public int numberOfExpenses() {
        return dataStore.count();
    }

    public void saveExpense(Expense expense) {
        dataStore.save(expense);
        expenses = null;
    }

    public List<Expense> getAllExpenses() {
        if (expenses == null) {
            expenses = dataStore.all();
        }
        return expenses;
    }

    public void remove(Expense expense) {
        dataStore.remove(expense);
        expenses = null;
    }

    public Expense findById(String id) {
        return dataStore.byId(id);
    }

}
