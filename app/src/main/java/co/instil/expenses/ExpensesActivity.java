package co.instil.expenses;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import co.instil.expenses.data.ExpensesService;
import co.instil.expenses.model.Expense;
import co.instil.expenses.settings.SettingsActivity;

public class ExpensesActivity extends AppCompatActivity implements ExpensesAdapterDelegate {

    private static final Integer EDIT_EXPENSE_REQUEST = 101;

    private ExpensesService expensesService;
    private ExpensesAdapter expenseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        configureAdapter();
        configureExpensesList();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editExpense(null);
            }
        });
    }

    private void configureAdapter() {
        expensesService = ((ExpensesApplication) getApplication()).getExpensesService();
        expenseAdapter = new ExpensesAdapter(this, expensesService);
    }

    private void configureExpensesList() {
        RecyclerView expensesList = (RecyclerView) findViewById(R.id.expensesList);
        expensesList.setLayoutManager(new LinearLayoutManager(this));
        expensesList.setHasFixedSize(true);
        expensesList.setAdapter(expenseAdapter);
        configureSwipeToDelete(expensesList);
    }

    private void configureSwipeToDelete(final RecyclerView expensesList) {
        ItemTouchHelper.SimpleCallback callback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                Expense expense = ((ExpensesViewHolder) viewHolder).getExpense();
                confirmBeforeDeleting(expense);
            }
        };
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(expensesList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_expenses, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void expenseWasSelected(Expense expense) {
        editExpense(expense);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_EXPENSE_REQUEST) {
            switch (resultCode) {
                case RESULT_OK:
                    expenseAdapter.notifyDataSetChanged();
                    break;
            }
        }
    }

    private void editExpense(Expense expense) {
        Intent intent = new Intent(this, ExpenseDetailActivity.class);
        if (expense != null) {
            intent.putExtra("expenseId", expense.getId());
        }
        startActivityForResult(intent, EDIT_EXPENSE_REQUEST);
    }

    private void confirmBeforeDeleting(final Expense expense) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.message_confirm_receipt_delete)
                .setPositiveButton(R.string.caption_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        expensesService.remove(expense);
                        expenseAdapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.caption_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        expenseAdapter.notifyDataSetChanged();
                    }
                });
        builder.create().show();
    }

}
