package co.instil.expenses;

import co.instil.expenses.model.Receipt;

/**
 * Created by Gareth on 10/04/2017.
 */

public interface ReceiptAdapterDelegate {

    void receiptWasSelected(Receipt receipt);

}
