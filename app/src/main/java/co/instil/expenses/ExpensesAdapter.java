package co.instil.expenses;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import co.instil.expenses.data.ExpensesService;
import co.instil.expenses.model.Expense;

class ExpensesAdapter extends RecyclerView.Adapter {

    private ExpensesAdapterDelegate delegate;
    private ExpensesService service;

    public ExpensesAdapter(ExpensesAdapterDelegate delegate, ExpensesService service) {
        this.delegate = delegate;
        this.service = service;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_expense_item, parent, false);
        return new ExpensesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Expense expense = service.getAllExpenses().get(position);
        ((ExpensesViewHolder) holder).prepareForDisplay(expense);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delegate.expenseWasSelected(expense);
            }
        });
    }

    @Override
    public int getItemCount() {
        return service.numberOfExpenses();
    }

}
