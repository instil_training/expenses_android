package co.instil.expenses;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.instil.expenses.data.ExpensesService;
import co.instil.expenses.model.Expense;
import co.instil.expenses.model.Receipt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ExpenseDetailActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener, ReceiptAdapterDelegate {

    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
    private final int RECEIPT_EDIT_REQUEST = 102;

    @BindView(R.id.title_text_field) TextView titleTextField;
    @BindView(R.id.date_text_field) TextView dateTextField;
    @BindView(R.id.receipts_list) RecyclerView receiptsList;

    private ExpensesService expensesService;
    private Expense expense;
    private ReceiptsAdapter receiptsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        expensesService = ((ExpensesApplication) getApplication()).getExpensesService();
        expense = getOrCreateNewExpense(getIntent());

        displayTitle();
        displayExpense();
        configureDatePicker();
        configureReceiptsList();
        configureReceiptAddButton();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_expense_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                saveExpense();
                return true;
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == RECEIPT_EDIT_REQUEST) {
            Receipt receipt = (Receipt) data.getSerializableExtra("receipt");
            expense.addOrUpdate(receipt);
            receiptsAdapter.notifyDataSetChanged();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        dateTextField.setText(String.format(Locale.getDefault(), "%02d-%02d-%d", dayOfMonth, month + 1, year));
    }

    @Override
    public void receiptWasSelected(Receipt receipt) {
        editReceipt(receipt);
    }

    private void displayTitle() {
        if (getIntent().hasExtra("expenseId")) {
            setTitle("Edit expense");
        } else {
            setTitle("Create new expense");
        }
    }

    private void displayExpense() {
        titleTextField.setText(expense.getTitle());
        dateTextField.setText(formatDate(expense.getDate()));
    }

    private void configureDatePicker() {
        dateTextField.setOnClickListener(new DateClickListener(this, this, expense.getDate()));
    }

    private void configureReceiptAddButton() {
        FloatingActionButton addReceiptButton = (FloatingActionButton) findViewById(R.id.fab);
        addReceiptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editReceipt(null);
            }
        });
    }

    private void configureReceiptsList() {
        receiptsList.setLayoutManager(new LinearLayoutManager(this));
        receiptsList.setHasFixedSize(true);
        receiptsAdapter = new ReceiptsAdapter(this, expense);
        receiptsList.setAdapter(receiptsAdapter);
        configureSwipeToDelete(receiptsList);
    }

    private void configureSwipeToDelete(RecyclerView list) {
        ItemTouchHelper.SimpleCallback callback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                Receipt receipt = ((ReceiptViewHolder) viewHolder).getReceipt();
                confirmBeforeDeleting(receipt);
            }
        };
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(receiptsList);
    }

    private void confirmBeforeDeleting(final Receipt receipt) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.message_confirm_receipt_delete)
                .setPositiveButton(R.string.caption_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        expense.remove(receipt);
                        receiptsAdapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.caption_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        receiptsAdapter.notifyDataSetChanged();
                    }
                });
        builder.create().show();
    }

    private Expense getOrCreateNewExpense(Intent intent) {
        if (intent.hasExtra("expenseId")) {
            String expenseId = intent.getStringExtra("expenseId");
            return expensesService.findById(expenseId);
        }
        return new Expense();
    }

    private void editReceipt(Receipt receipt) {
        Intent intent = new Intent(this, ReceiptDetailActivity.class);
        if (receipt != null) {
            intent.putExtra("receipt", receipt);
        }
        startActivityForResult(intent, RECEIPT_EDIT_REQUEST);
    }

    private void saveExpense() {
        expense.setTitle(titleTextField.getText().toString());
        Date date = parseDate(dateTextField.getText().toString());
        if (date != null) {
            expense.setDate(date);
        }
        expensesService.saveExpense(expense);
        setResult(RESULT_OK);
        finish();
    }

    private String formatDate(Date date) {
        return dateFormatter.format(date);
    }

    private Date parseDate(String dateString) {
        try {
            return dateFormatter.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }
}
