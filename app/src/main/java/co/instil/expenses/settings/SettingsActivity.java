package co.instil.expenses.settings;


import android.os.Bundle;
import android.support.v7.app.ActionBar;
import co.instil.expenses.AppCompatPreferenceActivity;

public class SettingsActivity extends AppCompatPreferenceActivity {

    public static final String SERVICE_URL_KEY = "pref_service_url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureActionBar();
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    private void configureActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
