package co.instil.expenses.network;

import java.util.List;

public class Value {

    private float value;
    private List<Vertex> vertices;

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public List<Vertex> getVertices() {
        return vertices;
    }

    public void setVertices(List<Vertex> vertices) {
        this.vertices = vertices;
    }
}
