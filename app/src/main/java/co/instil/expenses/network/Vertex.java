package co.instil.expenses.network;

/**
 * Created by Gareth on 10/04/2017.
 */

class Vertex {

    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
