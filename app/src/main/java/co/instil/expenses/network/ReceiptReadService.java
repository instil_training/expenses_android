package co.instil.expenses.network;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ReceiptReadService {

    @Multipart
    @POST("receipt")
    Observable<ReceiptReadResponse> readReceipt(@Part MultipartBody.Part receiptImage);

}
