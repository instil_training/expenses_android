package co.instil.expenses.network;

import java.util.List;

public class ReceiptReadResponse {

    private Value total;
    private List<Value> otherValues;

    public Value getTotal() {
        return total;
    }

    public void setTotal(Value total) {
        this.total = total;
    }

    public List<Value> getOtherValues() {
        return otherValues;
    }

    public void setOtherValues(List<Value> otherValues) {
        this.otherValues = otherValues;
    }
}

