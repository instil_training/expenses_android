package co.instil.expenses;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.instil.expenses.dependency.NetworkComponent;
import co.instil.expenses.model.Receipt;
import co.instil.expenses.network.ReceiptReadResponse;
import co.instil.expenses.network.ReceiptReadService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ReceiptDetailActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private static final int REQUEST_CAMERA_CAPTURE = 1001;
    private static final int REQUEST_CAMERA_PERMISSION = 1002;
    private static final int REQUEST_INTERNET_PERMISSION = 1003;
    private static final String TAG = "ReceiptDetail";

    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

    @Inject ReceiptReadService receiptReadService;

    @BindView(R.id.date_text_field) EditText dateField;
    @BindView(R.id.amount_text_field) EditText amountField;
    @BindView(R.id.location_text_field) EditText locationField;

    private File receiptImageFile;
    private Receipt receipt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_detail);

        ButterKnife.bind(this);

        receipt = getOrCreateNewReceipt(getIntent());
        displayTitle();
        displayReceipt();

        configureToolbar();
        configureNetworkService();
        configureDatePicker();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_receipt_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                saveReceipt();
                return true;
            case R.id.action_scan:
                requestCameraCapture();
                return true;
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestCameraCapture();
                }
                break;
            case REQUEST_INTERNET_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    processReceiptImage();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CAMERA_CAPTURE && resultCode == RESULT_OK) {
            processReceiptImage();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        dateField.setText(String.format(Locale.getDefault(), "%02d-%02d-%d", dayOfMonth, month + 1, year));
    }

    private Receipt getOrCreateNewReceipt(Intent intent) {
        if (intent.hasExtra("receipt")) {
            return (Receipt) intent.getSerializableExtra("receipt");
        }
        return new Receipt();
    }

    private void displayTitle() {
        if (getIntent().hasExtra("receipt")) {
            setTitle("Edit receipt");
        } else {
            setTitle("Create receipt");
        }
    }

    private void displayReceipt() {
        locationField.setText(receipt.getLocation());
        dateField.setText(formatDate(receipt.getDate()));
        amountField.setText(formatAmount(receipt.getAmount()));
    }

    private void processReceiptImage() {
        if (checkSelfPermission(Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] { Manifest.permission.INTERNET }, REQUEST_INTERNET_PERMISSION);
        } else {
            if (receiptImageFile == null) {
                Log.e(TAG, "There was no receipt image available for processing");
                return;
            }
            final ProgressDialog progressDialog = configureProgressDialog();
            progressDialog.show();
            final RequestBody receiptFileRequestBody = RequestBody.create(MediaType.parse("image/jpeg"), receiptImageFile);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", receiptImageFile.getName(), receiptFileRequestBody);
            receiptReadService.readReceipt(body)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<ReceiptReadResponse>() {
                        @Override
                        public void accept(@io.reactivex.annotations.NonNull ReceiptReadResponse receiptReadResponse) throws Exception {
                            float receiptAmount = receiptReadResponse.getTotal().getValue();
                            amountField.setText(String.format(Locale.getDefault(), "£%.2f", receiptAmount));
                            progressDialog.dismiss();
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(@io.reactivex.annotations.NonNull Throwable throwable) throws Exception {
                            Log.e(TAG, "There was an error thrown by the remote service", throwable);
                            progressDialog.dismiss();
                        }
                    });
        }
    }

    @NonNull
    private ProgressDialog configureProgressDialog() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.caption_scan_wait));
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        return progressDialog;
    }

    private void saveReceipt() {
        receipt.setLocation(locationField.getText().toString());
        Date date = parseDate(dateField.getText().toString());
        if (date != null) {
            receipt.setDate(date);
        }
        receipt.setAmount(parseAmount(amountField.getText().toString()));
        Intent resultIntent = new Intent();
        resultIntent.putExtra("receipt", receipt);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private String formatAmount(float amount) {
        return String.format(Locale.getDefault(), "£%.2f", amount);
    }

    private float parseAmount(String amountString) {
        if (amountString.startsWith("£")) {
            amountString = amountString.substring(1);
        }
        return Float.parseFloat(amountString);
    }

    private String formatDate(Date date) {
        return dateFormatter.format(date);
    }

    private Date parseDate(String dateString) {
        try {
            return dateFormatter.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }

    private void configureNetworkService() {
        NetworkComponent networkComponent = ((ExpensesApplication) getApplication()).getNetworkComponent();
        networkComponent.inject(this);
    }

    private void configureToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.title_activity_receipt_detail);
        }
    }

    private void configureDatePicker() {
        dateField.setOnClickListener(new DateClickListener(this, this, receipt.getDate()));
    }

    private void requestCameraCapture() {
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                receiptImageFile = getFileForImage();
                if (receiptImageFile != null) {
                    Uri imageFileUri = FileProvider.getUriForFile(this, "co.instil.expenses.FileProvider", receiptImageFile);
                    if (imageFileUri != null) {
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
                    }
                }
                startActivityForResult(cameraIntent, REQUEST_CAMERA_CAPTURE);
            }
        }
    }

    private File getFileForImage() {
        File storageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        try {
            return File.createTempFile("receipt", ".jpg", storageDirectory);
        } catch (IOException ioe) {
            Log.e(TAG, "Unable to create temporary file for receipt capture", ioe);
        }
        return null;
    }
}
